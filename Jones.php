<?php
$db = new mysqli("127.0.0.1:3306","root", "", "webapp");
if (mysqli_connect_errno()) {
  printf("Не удалось подключиться: %s\n", mysqli_connect_error());
  exit();
}

$lastName = "Jones";
$sql = "SELECT * From user where last_name = '".$lastName."'";
if ($result = $db->query($sql)){
  while ($row = $result->fetch_row()) {
    print_r( $row );
  }
}
?>


<!DOCTYPE html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="Poldoja.css">
    <link href="https://fonts.googleapis.com/css?family=Rambla&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <div class="container-fluid maincontainer">
        <!-- Header-->
        <div class="row" id="header">
            <div class="col-sm-8">
                <a href="mentorahome.html"><img id="mainlogo" src="mentora_logo.svg"></a>
            </div>
            <div class="col-sm-1 menucont">
                <a href="mentorahome.php" class="menutext">homepage</a>
            </div>
            <div class="col-sm-1 dropdown">
                <p class="dropbtn">language</p>
                    <div class="dropdown-content">
                        <a id="rounddd1" href="#">English</a>
                        <a id="squaredd"href="#">Eesti</a>
                        <a id="rounddd2"href="#">Pусский</a>
                    </div>
                </p>
            </div>
            <div class="col-sm-1 menucont">
                    <a href="mentoraaboutus.html" class="menutext">about us</a>
            </div>
        </div>

        <div class="row profilebackground">
            <img src="profilepiano.png" id="profilepiano" alt="">
        </div>

        <div class="row profiledata">
            <div class="col-sm-6 fillprofile">
                <div class="row picturequote">
                    <div class="col-sm-5">
                        <img src="mentormonth.png" align="right" alt="mentor of the month" class="profilepicture">
                    </div>
                    <div class="col-sm-7" id="quote">„I can help you to create beautiful websites“</div>
                </div>
                <div class="row maininfo">
                    <h1>
                        <?php
                        $sql = "SELECT * From user where last_name = '".$lastName."'";
                        if ($result = $db->query($sql)){
                        while ($row = $result->fetch_row()) {
                        echo '' . $row[3] . ' ' ;
                        echo ' ' . $row[4] . '';
                        }
                        }
                        ?>
                    </h1>
                    Programmer
                    <br>
                    <?php
                    $sql = "SELECT * From user where last_name = '".$lastName."'";
                    if ($result = $db->query($sql)){
                    while ($row = $result->fetch_row()) {
                    echo ''. $row[5] . ', Harjumaa ';
                    }
                    }
                    ?>
                </div> 
            </div>
            <div class="col-sm-6">
                <div class="addinfo">
                <?php 
                    $sql = "SELECT * From user where last_name = '".$lastName."'";
                    if ($result = $db->query($sql)){
                        while ($row = $result->fetch_row()) {
                            echo '<p>profession:' . $row[6] . '<p>';
                            echo '<p>study group age:' . $row[7] . '<p>';
                            echo '<p>study group level:' . $row[10] . '<p>';
                            echo '<p>language:' . $row[8] . '<p>';
                        }
                    }
                ?>
                </div>
            </div>
        </div>

        <div class="row descriptioninfo">
            <div class="col-sm-6">
                <div id="profilebio">
                    <h1>
                        <b>About me</b>
                    </h1>
                    <br>
                    <p id="biodescription">
                        <?php
                        $sql = "SELECT * From user where last_name = '".$lastName."'";
                        if ($result = $db->query($sql)){
                        while ($row = $result->fetch_row()) {
                        echo $row[9];
                        }
                        }
                        ?>
                    </p>
                </div>
            </div>
            <div class="col-sm-6">
                <div id="profiledetails">
                    <h1>
                        <b>I can help you with</b>
                    </h1>
                    <ul>
                        <li>HTML</li>
                        <li>JS</li>
                        <li>PHP </li>
                        <li>Wordpress </li>
                        <li>Graphic Design</li>
                    </ul>
                    <p>
                        I have my own studio in Tallinn, Vabaduse väljak 16<br>
                        Feel free to visit!
                    </p>
                </div>
            </div>
        </div>

        <div class="row portfolio">
            <div class="col-sm-6">
                <h1 id="myportfolio">
                    <b>My portfolio</b>
                </h1>
                <iframe id="video" src="https://www.youtube.com/embed/3gZC5763wYk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="col-sm-6">
                <iframe id="video" src="https://www.youtube.com/embed/vphWgqbF-AM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
        </div>

        <div class="row reviews">
            <div class="col-sm-3">
                <a type="button" id="reviewbutton" href="mailto:mentora@mentora.ee?Subject=Jaana%20review">
                    Leave a review
                </a>
            </div>
            <div class="col-sm-3">
                <div class="form-group" align="center">
                    <label for="comment">Natalja writes:</label>
                    <textarea class="form-control" rows="5" id="comment" readonly>Great lessons! Learned a lot!</textarea>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group" align="center">
                    <label for="comment">Martin writes:</label>
                    <textarea class="form-control" rows="5" id="comment" readonly>Had a great experience. If you want to learn piano I recomend to study with Jaana</textarea>
                </div>
            </div>
            <div class="col-sm-3">
            <div class="form-group" align="center">
                    <label for="comment">Riho writes:</label>
                    <textarea class="form-control" rows="5" id="comment" readonly>Jaana is funny and nice! :)</textarea>
                </div>
            </div>
        </div>

        <div class="row feedback" background="background_table.svg" style="background-image:url(background_table.svg);background-position:Center;">
        <h1 id="contactformhead"><b>Contact Me</b></h1>
            <form id="contactform">
                <div id="contactplace">
                <input type="text" name="name" placeholder="Your name" id="contactname"><br><br>
                <input type="text" name="email" placeholder="Your email" id="contactmail">
                </div>
                <input type="text" name="letter" id="contactletter" placeholder="How may I help you?">
                <input type="submit" value="send" id="contactsubmit">
            </form>
        </div>
    

    </div>
</body>
</html>