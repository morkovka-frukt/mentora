<?php
require "db.php";

$data=$_POST;
if( isset($data['dosignup']))
{
    //здесь регистрируем
    $errors = array();
    if( trim($data['email']) == '')
    {
        $errors[] = "Введите e-mail!";
    }

    if($data['password'] == '')
    {
        $errors[] = "Введите пароль!";
    }

    if(R::COUNT('user', "email = ?", array($data['email'])) > 0)
    {
        $errors[] = "Пользователь с таким email уже существует";
    }

    if( empty($errors)) 
    {
        //все хорошо, регестрируем
        $user = R::dispense('user');
        $user->email = $data['email'];
        $user->password = password_hash($data['password'], PASSWORD_DEFAULT);
        $user->firstName = $data['first_name'];
        $user->lastName = $data['last_name'];
        $user->city = $data['city'];
        $user->profession = $data['profession'];
        R::store($user);
        echo '<div style="color: green;"> Вы успешно зарегестрированы </div><hr>';
    } else
    {
        echo '<div style="color: red;">'.array_shift($errors).'</div><hr>';
    }
}
?>

<!DOCTYPE="html">
<html lan="en">
<body>
<form action="signup.php" method="POST">

<p>
    <strong> First name </strong>
    <input type="text" name="first_name" value="<?php echo @$data['first_name']; ?>">
</p>

<p>
    <strong> Last name </strong>
    <input type="text" name="last_name" value="<?php echo @$data['last_name']; ?>">
</p>

<p>
    <strong> Email </strong>
    <input type="email" name="email" value="<?php echo @$data['email']; ?>">
</p>

<p>
    <strong> Password </strong>
    <input type="password" name="password" value="<?php echo @$data['password']; ?>">
</p>

<p>
    <strong> Choose city </strong>
    <select id="city" name="city">
                    <option>Tallinn</option>
                    <option>Tartu</option>
                    <option>Pärnu</option>
    </select>
</p>

<p>
    <strong> Choose profession </strong>
    <select id="profession" name="profession">
                    <option>Languages</option>
                    <option>Science</option>
                    <option>Music</option>
                    <option>Fitness</option>
    </select>
</p>

<p>
    <button type="submit" name="dosignup" >Зарегистрироваться</button>
</p>

</form>
</body>
</html>