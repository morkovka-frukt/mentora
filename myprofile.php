<?php
require "db.php";

$data=$_POST;
if( isset($data['dochangeprofile']))
{
    $user = $_SESSION['logged_user'];
    $user->age = $data['age'];
    $user->language = $data['language'];
    $user->aboutme = $data['about_me'];
    $user->level = $data['level'];
    $user->profession = $data['profession'];
    R::store($user);

}
?>
<!DOCTYPE="html">
<html lan="en">
<body>

Привет, <?php echo $_SESSION['logged_user']->email; ?> ! Change data in your profile!

<form action="myprofile.php" method="POST">

<p>
    <strong> What age group do you teach? </strong>
    <select id="age" name="age">
                    <option>0-6years old</option>
                    <option>7-12years old</option>
                    <option>12-17years old</option>
                    <option>grown ups</option>
    </select>
</p>

<p>
    <strong> Teaching language </strong>
    <select id="language" name="language">
                    <option>english</option>
                    <option>estonian</option>
                    <option>russian</option>
                    <option>finnish</option>
    </select>
</p>

<p>
    <strong> Student level </strong>
    <select id="level" name="level">
                    <option>beginner</option>
                    <option>intermediate</option>
                    <option>advanced</option>
                    <option>expert</option>
    </select>
</p>

<p>
    <strong> Location </strong>
    <select id="city" name="city">
        <option>Harjumaa</option>
        <option>Hiiumaa</option>
        <option>Ida-Virumaa</option>
        <option>Jõgevamaa</option>
        <option>Järvamaa</option>
        <option>Lääne-Virumaa</option>
        <option>Läänemaa</option>
        <option>Põlvamaa</option>
        <option>Pärnumaa</option>                                                               
        <option>Raplamaa</option>
        <option>Saaremaa</option>
        <option>Tartumaa</option>
        <option>Valgamaa</option>
        <option>Viljandimaa</option>
        <option>Võrumaa</option>
    </select>
</p>

<p>
    <strong> Profession </strong>
    <select id="profession" name="profession">
        <option>Acting</option>
        <option>Business</option>
        <option>Coreography</option>
        <option>IT</option>
        <option>Music</option>
        <option>Web-Design</option>
        <option>Science</option>
    </select>
</p>

<p>
    <strong> About me </strong>
    <input type="textarea" name="about_me" value="<?php echo @$data['about_me']; ?>">
</p>

<p>
    <button type="submit" name="dochangeprofile" >Войти</button>
</p>

</form>
</body>
</html>