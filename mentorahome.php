<?php
require "db.php";


$data=$_POST;
if( isset($data['do_login']))
{
    $errors = array();
    $user = R::findOne('user', 'email = ?', array($data['email']));
    if( $user )
    {
        //login exists
        if( password_verify($data['password'], $user->password)){
            //everything ok! login user
            $_SESSION['logged_user'] = $user;
            header('Location: myprofile.php');
            echo '<div style="color: green;"> You are sucessfully logged!<br>Можете перейти на <a href="index.php">главную</a> страницу! </div><hr>
            <br>или на страницу <a href="myprofile.php">профиля</a> страницу! </div><hr>';

        } else
        {
            $errors[] = 'Wrong password!';
        }
    } else
    {
        $errors[] ='User does not exist!';
    }

    if( ! empty($errors))
    {
        echo '<div style="color: red;">'.array_shift($errors).'</div><hr>';
    }


}


if( isset($data['do_signup']))
{
    //здесь регистрируем
    $errors = array();
    if( trim($data['email']) == '')
    {
        $errors[] = "Insert e-mail!";
    }

    if($data['password'] == '')
    {
        $errors[] = "Insert password!";
    }

    if($data['first_name'] == '')
    {
        $errors[] = "Insert firstname!";
    }

    if($data['last_name'] == '')
    {
        $errors[] = "Insert lastname!";
    }

    if(R::COUNT('user', "email = ?", array($data['email'])) > 0)
    {
        $errors[] = "Пользователь с таким email уже существует";
    }

    if( empty($errors)) 
    {
        //все хорошо, регестрируем
        $user = R::dispense('user');
        $user->email = $data['email'];
        $user->password = password_hash($data['password'], PASSWORD_DEFAULT);
        $user->firstName = $data['first_name'];
        $user->lastName = $data['last_name'];
        $user->city = $data['city'];
        $user->profession = $data['profession'];
        R::store($user);
        echo '<div style="color: green;"> You are sucesfully registered </div><hr>';
    } else
    {
        echo '<div style="color: red;">'.array_shift($errors).'</div><hr>';
    }
}

if( isset($_SESSION['logged_user'])) {
    header("location:myprofile.php");
}

?>


<html>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="mentorahome.css">
    <link href="https://fonts.googleapis.com/css?family=Rambla&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
<!--PHP -->


<!--Header-->
    <header>
        <div id="headerright">
            <p data-toggle="modal" data-target="#myModal"><a href="#myModal" class="headertext" id="forteachers">for teachers</a></p>
            <div class="dropdown">
                    <p class="dropbtn">language</p>
                    <div class="dropdown-content">
                    <a id="rounddd1" href="#">English</a>
                    <a id="squaredd"href="#">Eesti</a>
                    <a id="rounddd2"href="#">Pусский</a>
                    </div>
                  </div>
            <p><a href="mentoraaboutus.html" class="headertext" id="aboutus">about us</a></p>
        </div>
        <div>
            <a href="mentorahome.html"><img id="mainlogo" src="mentora_logo.svg"></a>
        </div>
    </header>
<!--Searchbar-->
    <div>
        <div>
            <img src="background_table.svg">
            <a href="mentorasearch.html"><button type="button" id="startbutton">Click here to get started!</button></a>
        </div>
        <div id="homeheading">
            <h1>Find the right personal trainer, teacher or mentor,<br>with the best price</h1>
        </div>
    </div>
<!--Modal-->
    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <img src="mentora_logo.svg">
                    <button type="button" class="close" data-dismiss="modal" data-toggle="tooltip" data-placement="top" title="Close">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="vl"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <h5 style="text-align: center">create new account</h5>
                                <br>
                                <form action="mentorahome.php" method="POST">
                                        <div class="form-row">
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" placeholder="first_name" name="first_name" value="<?php echo @$data['first_name']; ?>">
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" placeholder="last_name" name="last_name" value="<?php echo @$data['last_name']; ?>">
                                                </div>
            
                                                <div class="w-100"></div><br>
            
                                                <div class="col-sm-12">
                                                    <input type="email" class="form-control" placeholder="email" name="email" value="<?php echo @$data['email']; ?>">
                                                </div>
            
                                                <div class="w-100"></div><br>
            
                                                <div class="col-sm-12">
                                                    <input type="password" class="form-control" placeholder="password" name="password">
                                                </div>
            
                                                <div class="w-100"></div><br>
            
                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <select class="form-control"
                                                            aria-label="Text input with segmented dropdown button"
                                                            placeholder="county" name="city">
                                                        <div class="input-group-append">
                                                            <button type="button"
                                                                class="btn btn-outline-secondary dropdown-toggle dropdown-toggle-split"
                                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            </button>
                                                            <div class="dropdown-menu">
                                                                <option>Harjumaa</option>
                                                                <option>Hiiumaa</option>
                                                                <option>Ida-Virumaa</option>
                                                                <option>Jõgevamaa</option>
                                                                <option>Järvamaa</option>
                                                                <option>Lääne-Virumaa</option>
                                                                <option>Läänemaa</option>
                                                                <option>Põlvamaa</option>
                                                                <option>Pärnumaa</option>                                                               
                                                                <option>Raplamaa</option>
                                                                <option>Saaremaa</option>
                                                                <option>Tartumaa</option>
                                                                <option>Valgamaa</option>
                                                                <option>Viljandimaa</option>
                                                                <option>Võrumaa</option>
                                                            </div>
                                                        </div>
                                                        </select>                                                    
                                                    </div>
                                                </div>
            
                                                <div class="w-100"></div><br>
            
                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <select type="text" class="form-control"
                                                            aria-label="Text input with segmented dropdown button"
                                                            placeholder="profession" name="profession">
                                                        <div class="input-group-append">
                                                            <button type="button"
                                                                class="btn btn-outline-secondary dropdown-toggle dropdown-toggle-split"
                                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            </button>
                                                            <div class="dropdown-menu">
                                                                <option>Acting</option>
                                                                <option>Business</option>
                                                                <option>Coreography</option>
                                                                <option>IT</option>
                                                                <option>Music</option>
                                                                <option>Web-Design</option>
                                                                <option>Science</option>
                                                            </div>
                                                        </div>
                                                        </select>
                                                    </div>
            
                                                    <div class="w-100"></div><br>
            
                                                    <div>
                                                        <button id="regbutton" type="submit" class="btn btn-primary" name="do_signup">sign up</button>
                                                    </div>
                                                </div>
                                        </div>
                                </form>
                            </div>
                            <div class="col-md-6 ml-auto">
                                <h5 style="text-align: center">log in</h5>
                                <br>
                                <form action="" method="POST">
                                        <div class="form-row">
                                                <div class="col-sm-12">
                                                    <input type="text" class="form-control" placeholder="email" name="email">
                                                </div>

                                                <div class="w-100"></div><br>

                                                <div class="col-sm-12">
                                                    <input type="password" class="form-control" placeholder="password" name="password">
                                                    <p id="passwordreset"><a id="pwlink" href="">forgot your password?</a></p>
                                                </div>
                                                <br>       
                                                <div class="col-sm-12">
                                                    <div>
                                                        <button id="regbutton" type="submit" class="btn btn-primary" name="do_login">login</button>
                                                    </div>
                                                </div>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--Mentor Of the Month-->
    <div id="mentorofthemonth">
        <div>
            <h1 id="additionaltext" style="color: white; position: relative; top: 30px">Mentor of the month</h1>
            <div id="mentormonthimg">
                <img src="mentormonth.png">
            </div>
            
            <div class="mentormonthcontent">
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
            </div>    
        </div>
    </div>
<!--Upcoming Workshops-->
    <div id="additionaldiv" style="z-index: 1">
        <h1 id="additionaltext">Upcoming workshops</h1>
    </div>
    <div id="workshops">
        
        <div style="z-index: -1" class="bd-example">
            <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                    <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                  </ol>
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img src="focus.jpg" class="d-block w-100" alt="..." width="1440" height="600" style="opacity: 0.5;">
                      <div class="carousel-caption d-none d-md-block">
                        <h5 class="carouseltext">Developing unstoppable focus</h5>
                        <p class="carouseltext">Learn how to unlock the full power of concentration</p>
                      </div>
                    </div>
                    <div class="carousel-item">
                      <img src="martial.jpg" class="d-block w-100" alt="..." width="1440" height="600" style="opacity: 0.5;">
                      <div class="carousel-caption d-none d-md-block">
                        <h5 class="carouseltext">The art of meditation</h5>
                        <p class="carouseltext">Learn how to get your mind to an elevated level</p>
                      </div>
                    </div>
                    <div class="carousel-item">
                      <img src="teamwork.jpg" class="d-block w-100" alt="..." width="1440" height="600" style="opacity: 0.5;">
                      <div class="carousel-caption d-none d-md-block">
                        <h5 class="carouseltext">Team + Work + Flow</h5>
                        <p class="carouseltext">Achive the best results working with eachother</p>
                      </div>
                    </div>
                  </div>
                  <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
            </div>
        </div>
    </div>
<!--Footer-->
    <footer id="footerbackground">
        <div>
            <h5 class="footertext">Vabaduse väljak 6 <br> Tallinn 80068, Estonia</h5>
        </div>
        <div id="contacts">
            <p class="footertext"><a class="footertext" href="mailto:info@mentora.ee?Subject=Hello%20again" 
                target="_top">info@mentora.ee</a> <br>+372 5982 2532<br>©Mentora OÜ 2019</p>
        </div>
        <div>
            <a href="mentorahome.html"><img id="bottomlogo" src="mentora_logo_white.svg"></a>
        </div>
    </footer>
<!--Bootstrap-->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
        </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
        </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
        </script>
</body>

</html>